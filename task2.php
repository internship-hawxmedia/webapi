<?php
  require_once __DIR__ . '/config.php';

  include_once 'libs/php-jwt-master/src/BeforeValidException.php';
  include_once 'libs/php-jwt-master/src/ExpiredException.php';
  include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
  include_once 'libs/php-jwt-master/src/JWT.php';
  use \Firebase\JWT\JWT;
  class API {
    function loginApi($jwt){
		try {
			$secret = "nId03u4yyYoeUbMkWYyqy2t_mSQISww7tSSi68Hc-nVRTSHI6kdKAJ9FDjmBddU5OnPPDZDSd5Gzh0u9CRwZ4TGqcJM2Wt4vEpICyGRbFRJF0S0p2gltHNx3zSS-a3bj0NbmXk1Qu1q5-p-wRhth_vknk0Jiq5QgERAfYWisQbPbRuxk5B5rVb-ySHqdRtzj7fTRiAORJGKTzImgn-TPTbqh9QhMPhIoYooREzzsIqS01a32hM8WphNd7pjp4wRsIuLavYP8vXvj3xBpsx4XJM33e9e0sv46yfwxhMrgqLlfGCVqphAAUdN59KsZ51Asm8c5-aRdMdatUOvyAnq8IQ";
			$token = JWT::decode($jwt,$secret, array('HS256'));
			$eid = $token->data->id;
			require_once __DIR__. '/config.php';
			$db = new Connect;
			$user = array();
			$data = $db->prepare("SELECT e.id, e.user_name, e.fname, e.lname, e.employee_code, d.department, ds.designation, e.phone, e.email from sys_employee e, sys_department d, sys_designation ds where e.designation = ds.did and e.department = d.id and e.id = '".$eid."'");
			$data->execute();
			while($OutputData = $data->fetch(PDO::FETCH_ASSOC)){
				$user[$OutputData['id']] = array(
					'id' => $OutputData['id'],
					'user_name' => $OutputData['user_name'],
					'fname' => $OutputData['fname'],
					'lname' => $OutputData['lname'],
					'ecode' => $OutputData['employee_code'],
					'dept' => $OutputData['department'],
					'desg' => $OutputData['designation'],
					'phone' => $OutputData['phone'],
					'email' => $OutputData['email']
				);
			}
			if($user == [])
				return "User data not found.";
			return json_encode($user);	
		}
		catch(Firebase\JWT\SignatureInvalidException $e){
			echo "Signature verification failed.";
		}
		
    }

  }
//

  if ($_SERVER['REQUEST_METHOD']=='POST') {
	  $jwt=$_POST['jwt'];
	  $API = new API;
	  header('Content-Type: application/json');
	  echo $API->loginApi($jwt);
	}
 ?>
