<?php
  require_once __DIR__ . '/config.php';

  include_once 'libs/php-jwt-master/src/BeforeValidException.php';
  include_once 'libs/php-jwt-master/src/ExpiredException.php';
  include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
  include_once 'libs/php-jwt-master/src/JWT.php';
  use \Firebase\JWT\JWT;
  class API {
    function loginApi($name,$pass){
      $db = new Connect;
      $users= array();
      //can use this if the password is not encrypted
    //  $pass = sha1($pass);
      $data = $db->prepare("select id,fname,lname,password from sys_employee where user_name='$name'");
      $data->execute();
      while($OutputData = $data->fetch(PDO::FETCH_ASSOC)){
        $id= $OutputData['id'];
        $fname= $OutputData['fname'];
        $lname= $OutputData['lname'];
        $password= $OutputData['password'];

        if(strcmp($pass,$password)==0)
        {
          $secret_key = "nId03u4yyYoeUbMkWYyqy2t_mSQISww7tSSi68Hc-nVRTSHI6kdKAJ9FDjmBddU5OnPPDZDSd5Gzh0u9CRwZ4TGqcJM2Wt4vEpICyGRbFRJF0S0p2gltHNx3zSS-a3bj0NbmXk1Qu1q5-p-wRhth_vknk0Jiq5QgERAfYWisQbPbRuxk5B5rVb-ySHqdRtzj7fTRiAORJGKTzImgn-TPTbqh9QhMPhIoYooREzzsIqS01a32hM8WphNd7pjp4wRsIuLavYP8vXvj3xBpsx4XJM33e9e0sv46yfwxhMrgqLlfGCVqphAAUdN59KsZ51Asm8c5-aRdMdatUOvyAnq8IQ";

            $issuer_claim = "localhost"; // this can be the servername
            $issuedat_claim = time(); // issued at
            $notbefore_claim = $issuedat_claim + 10; //not before in seconds
            $expire_claim = $issuedat_claim + 60; // expire time in seconds
            $token = array(
                "iss" => $issuer_claim,
                "iat" => $issuedat_claim,
                "nbf" => $notbefore_claim,
                "exp" => $expire_claim,
                "data" => array(
                    "id" => $id,
                    "firstname" => $fname,
                    "lastname" => $lname,
                    "user_name" => $name
            ));

            http_response_code(200);

            $jwt = JWT::encode($token, $secret_key);
            return json_encode(
                array(
                    "message" => "Successful login.",
                    "jwt" => $jwt,
                    "user_name" => $name,
                    "expireAt" => $expire_claim
                ));


        }
        else{

            http_response_code(401);
            return json_encode(array("message" => "Login failed.", "password" => $password));
        }
    }

      }
      function userPropertyApi($Token){
        $Tok = json_decode($Token);
        //var_dump($Tok);
        $jwt= $Tok->jwt;
        $secret_key = "nId03u4yyYoeUbMkWYyqy2t_mSQISww7tSSi68Hc-nVRTSHI6kdKAJ9FDjmBddU5OnPPDZDSd5Gzh0u9CRwZ4TGqcJM2Wt4vEpICyGRbFRJF0S0p2gltHNx3zSS-a3bj0NbmXk1Qu1q5-p-wRhth_vknk0Jiq5QgERAfYWisQbPbRuxk5B5rVb-ySHqdRtzj7fTRiAORJGKTzImgn-TPTbqh9QhMPhIoYooREzzsIqS01a32hM8WphNd7pjp4wRsIuLavYP8vXvj3xBpsx4XJM33e9e0sv46yfwxhMrgqLlfGCVqphAAUdN59KsZ51Asm8c5-aRdMdatUOvyAnq8IQ";
        JWT::$leeway = 60;
        $decoded = JWT::decode($jwt, $secret_key, array('HS256'));
        $uname = $decoded->data->user_name;
    		$db = new Connect;
    		$users = array();
    		$data = $db->prepare("SELECT e.id, e.user_name, e.fname, e.lname, e.employee_code, d.department, ds.designation, e.phone, e.email from sys_employee e, sys_department d, sys_designation ds where e.designation = ds.did and e.department = d.id and e.user_name= '$uname'");
    		$data->execute();
    		while($OutputData = $data->fetch(PDO::FETCH_ASSOC)){
    				  $users[] = $OutputData;
    		}
    		if($users == [])
    		    return "User data not found.";
    		return json_encode($users);
    	}

      function checkInOutApi($Token){
        $db = new Connect;
        $users= array();
        $Tok = json_decode($Token);
        //var_dump($Tok);
        $jwt= $Tok->jwt;
        $secret_key = "nId03u4yyYoeUbMkWYyqy2t_mSQISww7tSSi68Hc-nVRTSHI6kdKAJ9FDjmBddU5OnPPDZDSd5Gzh0u9CRwZ4TGqcJM2Wt4vEpICyGRbFRJF0S0p2gltHNx3zSS-a3bj0NbmXk1Qu1q5-p-wRhth_vknk0Jiq5QgERAfYWisQbPbRuxk5B5rVb-ySHqdRtzj7fTRiAORJGKTzImgn-TPTbqh9QhMPhIoYooREzzsIqS01a32hM8WphNd7pjp4wRsIuLavYP8vXvj3xBpsx4XJM33e9e0sv46yfwxhMrgqLlfGCVqphAAUdN59KsZ51Asm8c5-aRdMdatUOvyAnq8IQ";
        JWT::$leeway = 60;
        $decoded = JWT::decode($jwt, $secret_key, array('HS256'));
        $uname = $decoded->data->user_name;
        $data = $db->prepare("select DISTINCT sa.clock_in,sa.clock_out from sys_attendance as sa,sys_employee as se where se.user_name= '$uname'");
        $data->execute();
        while($OutputData = $data->fetch(PDO::FETCH_ASSOC)){
          if(strcmp(strtolower($OutputData['clock_in']),null) != 0 &&  strcmp(strtolower($OutputData['clock_out']),null) != 0){
            $users[] = array(
              'CheckIn' => $OutputData['clock_in'],
              'CheckOut' => $OutputData['clock_out'],
              'Difference' => round(abs(strtotime($OutputData['clock_in']) - strtotime($OutputData['clock_out']) ) / 60,2)." Minute"
            );
          }
          else {
            $users[] = array(
              'CheckIn' => $OutputData['clock_in'],
              'CheckOut' => $OutputData['clock_out'],
              'Difference' => "Cannot Be Calculated"
            );
          }

        }
        return json_encode($users);
      }


  }



  // $user='admin';
  // $pass='$2y$10$UK119sCon2/x7Mit9bdgNuJjskxbIckm4mGdCxx5wlil/Cxa0kqI.';
  if ($_SERVER['REQUEST_METHOD']=='POST') {

  $user=$_POST['user_name'];
  $pass=$_POST['password'];

  $API = new API;
  header('Content-Type: application/json');
  echo "\nTask 1\n";
  $Token = $API->loginApi($user,$pass);
  echo $Token;

   echo "\nTask 2\n";
  $uDetails = $API->userPropertyApi($Token);
  echo $uDetails;

  echo "\nTask 3\n";
 $Timming = $API->checkInOutApi($Token);
 echo $Timming;



}






 ?>
